void main() {
  int n = 10;
  int result = fibonacci(n);
  print('Số Fibonacci thứ $n là: $result');
}

int fibonacci(int n) {
  if (n <= 1) {
    return n;
  } else {
    return fibonacci(n - 1) + fibonacci(n - 2);
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        systemOverlayStyle:
            const SystemUiOverlayStyle(statusBarColor: Colors.black),
        backgroundColor: Colors.blue,
        title: const Center(
            child: Text(
          'Danh sách',
          style: TextStyle(
              fontSize: 26, fontWeight: FontWeight.bold, color: Colors.white),
        )),
      ),
      body: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          if (constraints.maxWidth < 480) {
            return Row(
              children: [
                Expanded(
                  child: ListView.builder(
                    itemCount: 3,
                    itemBuilder: (context, index) {
                      return ListTile(
                        title: Center(
                            child: Text(
                          'Person $index',
                          style: const TextStyle(
                              fontSize: 24, color: Colors.green),
                        )),
                        onTap: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  DetailPage(selectedItemIndex: index)),
                        ),
                      );
                    },
                  ),
                ),
              ],
            );
          } else {
            return Row(
              children: [
                Expanded(
                  flex: 1,
                  child: ListView.builder(
                    itemCount: 3,
                    itemBuilder: (context, index) {
                      return ListTile(
                        title: Center(
                          child: Text(
                            'Person $index',
                            style: const TextStyle(
                                fontSize: 24, color: Colors.green),
                          ),
                        ),
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) =>
                                  DetailPage(selectedItemIndex: index),
                            ),
                          );
                        },
                      );
                    },
                  ),
                ),
                const VerticalDivider(
                  thickness: 1,
                  color: Colors.grey,
                ),
                const Expanded(
                  flex: 2,
                  child: DetailPageHome(),
                ),
              ],
            );
          }
        },
      ),
    );
  }
}

class DetailPage extends StatelessWidget {
  final int selectedItemIndex;

  const DetailPage({super.key, required this.selectedItemIndex});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        systemOverlayStyle:
            const SystemUiOverlayStyle(statusBarColor: Colors.black),
        backgroundColor: Colors.blue,
        title: const Center(
          child: Text('Person',
              style: TextStyle(
                  fontSize: 26,
                  fontWeight: FontWeight.bold,
                  color: Colors.white)),
        ),
      ),
      body: const Column(children: [
        Center(child: Text('Họ tên: Person')),
        Text('Trang thái: Online '),
        Padding(
          padding: EdgeInsets.only(left: 20.0, right: 20.0),
          child:
              Text('Gioi thieu: If the child should be smaller than the parent,'
                  ' consider wrapping the child. '),
        )
      ]),
    );
  }
}

class DetailPageHome extends StatelessWidget {
  const DetailPageHome({super.key});

  @override
  Widget build(BuildContext context) {
    return const Column(children: [
      Center(child: Text('Họ tên: Person')),
      Text('Trang thái: Online '),
      Padding(
        padding: EdgeInsets.only(left: 20.0, right: 20.0),
        child:
            Text('Gioi thieu: If the child should be smaller than the parent, '
                'consider wrapping the child. '),
      )
    ]);
  }
}

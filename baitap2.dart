import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Kiểm tra số nguyên tố',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final TextEditingController _numberController = TextEditingController();
  bool _isPrime = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Kiểm tra số nguyên tố'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TextField(
              controller: _numberController,
              decoration: const InputDecoration(hintText: 'Nhập số bất kỳ'),
            ),
            ElevatedButton(
              onPressed: () {
                setState(() {
                  _isPrime = _checkPrime(_numberController.text);
                });
              },
              child: const Text('Kiểm tra'),
            ),
            Text(_isPrime
                ? 'Số ${_numberController.text} là số nguyên tố'
                : 'Số ${_numberController.text} không phải là số nguyên tố'),
          ],
        ),
      ),
    );
  }

  bool _checkPrime(String number) {
    int n = int.parse(number);
    if (n <= 1) return false;
    for (int i = 2; i * i <= n; i++) {
      if (n % i == 0) return false;
    }
    return true;
  }
}
